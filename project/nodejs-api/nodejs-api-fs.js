
'use strict';

const Fs = require('fs');


class NodejsApiFs {
  constructor(actor) {
    return new Proxy(this, {
      get: (target, property, receiver) => {
        if('function' === typeof target[property]) {
          return function*(...args) {
            yield* actor.callback((cb) => {
              const method = Reflect.get(Fs, property);
              method(...args, (err) => {
                ddb.error(...args, err);
                cb(err);
              });
            });
          };
        }
      }
    });
  }
  
  *access(){}
  *appendFile(){}
  *chmod(){}
  *chown(){}
  *close(){}
  *copyFile(){}
  *exists(){}
  *fchmod(){}
  *fchown(){}
  *fdatasync(){}
  *fstat(){}
  *fsync(){}
  *ftruncate(){}
  *futimes(){}
  *lchmod(){}
  *lchown(){}
  *lutimes(){}
  *link(){}
  *lstat(){}
  *mkdir(){}
  *mkdtemp(){}
  *open(){}
  *opendir(){}
  *read(){}
  *rea(){}
  *readdir(){}
  *readFile(){}
  *readlink(){}
  *readv(){}
  *realpath(){}
  *rename(){}
  *rmdir(){}
  *rm(){}
  *stat(){}
  *symlink(){}
  *truncate(){}
  *unlink(){}
  *utimes(){}
  *write(){}
  *write(){}
  *writeFile(){}
  *writev(){}
}

module.exports = NodejsApiFs;
